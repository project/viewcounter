<?php
/// $Id$
/// @author Michael Moritz mimo/at/gn.apc.org

/**
 * Form builder function for module settings.
 */
function viewcounter_admin_overview() {
  // uncomment to test and run on next cron in any case
//   variable_set('gnbilling_custom_viewcounter_lastrun', 0);
  $viewcounter = variable_get('viewcounter', array());
  $form['viewcounter'] = array(
    '#type' => 'fieldset',
    '#title' => t('View counters configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => ($viewcounter['vid'] > 0),
    '#description' => t('View counters are useful for counting the number of rows in a view.'),
  );
  $form['viewcounter']['viewcounter_how_often'] = array(
    '#type' => 'radios',
    '#title' => t('How often should counters be created'),
    '#default_value' => ($viewcounter['how_often'] ? $viewcounter['how_often'] : 'monthly'),
    '#options' => array(
      'daily' => t('Daily'),
      'weekly' => t('Weekly'),
      'monthly' => t('Monthly')),
  );
  $lastrun = variable_get('viewcounter_lastrun', 0);
  $form['viewcounter']['lastrun'] = array(
    '#type' => 'textfield',
    '#title' => t('Last run'),
    '#default_value' => format_date($lastrun, 'custom', 'Y-m-d H:i:s O'),
    '#maxlength' => 25,
//     '#disabled' => true,
  );
  $vocs = taxonomy_get_vocabularies();
  $vids = array(0 => '<'. t('None') .'>');
  foreach ($vocs as $vid => $vocab) {
    $vids[$vid] = $vocab->name;
  }
//   $vid = variable_get('viewcounter_vocabulary', 0);
//   $form['viewcounter'] = array('#tree' => true);
  $form['viewcounter']['viewcounter_vid'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary'),
    '#description' => t('Select the vocabulary to use for viewcounters. The vocabulary needs to have at least one term in it. Thsi will be used to identify the counters created by viewcounter.'),
    '#default_value' => $viewcounter['vid'],
    '#options' => $vids,
    '#required' => true,
  );
  $tree = taxonomy_get_tree($viewcounter['vid']);
//   dsm($tree);
  $viewcounter['instances'] = ($viewcounter['instances'] ?
    $viewcounter['instances'] : array());
  foreach($viewcounter['instances'] as $index => $instance) {
    $id = $instance['vcid'];
    $ids[$id] = '';
    if ($instance['enabled']) {
      $enabled[] = $id;
    }
    $form['id'][$id] = array('#value' => $id);
//     $form['enabled'][$id] = array('#value' => $instance['enabled'], '#type'=>'checkbox');
    $term = taxonomy_get_term($instance['tid']);
    $form['tag'][$id] = array('#value' => $term->name);
    list($module, $choice) = explode("/", $instance['argtype'], 2);
    $form['module'][$id] = array('#value' => $module);
    $form['type'][$id] = array('#value' => $choice);
    $form['view'][$id] = array('#value' => $instance['countview']);
    $ops = array('edit' => t('edit'), 'preview' => t('preview'), 'run' => t('run'),
      'export' => t('export'), 'delete' => t('delete'));
    foreach($ops as $k=>$v) {
      $form['ops'][$id][$k] = array('#value' => l($v,
        'admin/settings/viewcounter/'. $k . '/'. $id));
    }
  }
  $form['ids'] = array('#type' => 'checkboxes', '#options' => $ids,
    '#default_value' => (is_array($enabled) ? $enabled : NULL));

  $form['form_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}
function viewcounter_admin_overview_submit($form_id, &$form_values) {
  $viewcounter = variable_get('viewcounter', array());
  $viewcounter['vid'] = $form_values['viewcounter_vid'];
  $viewcounter['how_often'] = $form_values['viewcounter_how_often'];
  foreach($viewcounter['instances'] as $index => $inst) {
    $viewcounter['instances'][$index]['enabled'] = ($form_values['ids'][$inst['vcid']] == 0 ? 0 : 1);
  }
//   dsm($viewcounter);
  $lastrun = strtotime($form_values['lastrun']);
  if ($lastrun && ($lastrun != variable_get('viewcounter_lastrun', 0))) {
    variable_set('viewcounter_lastrun', $lastrun);
    drupal_set_message("Date of last run has been changed.");
  }
  variable_set('viewcounter', $viewcounter);
  drupal_set_message("Settings have been saved.");
//   dsm($form_values);
}
function theme_viewcounter_admin_overview($form) {
//   dsm($form);
  $rows = array();
  if (isset($form['id']) && is_array($form['id'])) {
    $colspan = 0;
    foreach (element_children($form['id']) as $key) {
      $row = array();
//       $row[] = drupal_render($form['enabled'][$key]);
      $row[] = drupal_render($form['ids'][$key]);
      $row[] = drupal_render($form['id'][$key]);
      $row[] = drupal_render($form['tag'][$key]);
      $row[] = drupal_render($form['module'][$key]);
      $row[] = drupal_render($form['type'][$key]);
      $row[] = drupal_render($form['view'][$key]);
      $count = 0;
      foreach(element_children($form['ops'][$key]) as $op ) {
        $row[] = drupal_render($form['ops'][$key][$op]);
        $count++;
        $colspan = max($colspan, $count);
      }
      $rows[] = $row;
    }
  }
  $header = array(t('Enabled'), t('Id'), t('Tag'), t('Argument module'),
    t('Argument type'), t('View'), array('data' => t('Operations'), 'colspan' => $colspan));
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}
function viewcounter_delete_confirm($vcid) {
 $form['vcid'] = array('#type' => 'value', '#value' => $vcid);

 return confirm_form($form,
   t('Are you sure you want to delete viewcounter #@vc?', array('@vc' => $vcid)),
   isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/viewcounter',
   t('This action cannot be undone.'),
   t('Delete'), t('Cancel'));
}
function viewcounter_delete_confirm_submit($form_id, $form_values) {
  if ($form_values['confirm']) {
//     viewcounter_delete(
    _viewcounter_delete($form_values['vcid']);
  }
  return 'admin/settings/viewcounter';
}
function viewcounter_run_confirm($vcid) {
 $form['vcid'] = array('#type' => 'value', '#value' => $vcid);

 return confirm_form($form,
   t('Are you sure you want to run viewcounter #@vc?', array('@vc' => $vcid)),
   isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/viewcounter',
   t('This action will update or create the viewcounters in this set with the '
   . 'current counts. Normally, this is run by cron.'),
   t('Run'), t('Cancel'));
}
function viewcounter_run_confirm_submit($form_id, $form_values) {
  if ($form_values['confirm']) {
//     viewcounter_delete(
    viewcounter_run($form_values['vcid'], false /* ignores whether enabled */);
  }
  return 'admin/settings/viewcounter';
}
function viewcounter_export($vcid) {
 $inst = _viewcounter_load($vcid);
 $form['export'] = array('#type' => 'textarea', '#value' => serialize($inst));
 return $form;
}

function viewcounter_form_edit($vcid) {
  $inst = _viewcounter_load($vcid);
  $inst['form_step'] = array("#value" => 999);
  $form['form_finished'] = true;
  drupal_set_title(t("Editing viewcounter #@vcid", array("@vcid" => $vcid)));
  return viewcounter_form_add($inst);
//   return drupal_get_form('viewcounter_form_add', $inst);
}
function viewcounter_form_edit_submit($form_id, &$form_values = NULL) {
  return viewcounter_form_add_submit($form_id, $form_values);
}
function viewcounter_form_preview($vcid) {
  $inst = _viewcounter_load($vcid);
  list($module, $type) = explode("/", $inst['argtype']);
  $args = module_invoke($module, "viewcounter_process_$type", $inst);
  $counters = _viewcounter_count_from_view_with_args($inst['countview'], $args, _viewcounter_explode_args($inst['countview_arguments']));
  if (function_exists("krumo")) {
    dsm($args);
    dsm($counters);
  } else {
    drupal_set_message(t("Found @no arguments and got @rno results. Install devel+krumo to get previews."), array('@no' => count($args), '@rno' => count($counters)));
  }
  return array();
}
function viewcounter_form_add($form_values = NULL) {
//   dsm($form_values);
  $viewcounter = variable_get('viewcounter', array());
  if (!$viewcounter['vid']) {
    form_set_error('', t("You must <a href=\"@link\">select a vocabulary</a> first.",
      array("@link" => url('admin/settings/viewcounter'))));
    return;
  }
  $form = array();
  $form['#multistep'] = TRUE;
  $form['#redirect'] = FALSE;

  $tree = taxonomy_get_tree($viewcounter['vid']);
  foreach($tree as $term) {
    $terms[$term->tid] = $term->name;
  }
  $form['tid'] = array(
    '#type' => 'radios',
    '#title' => t("Tag"),
    '#description' => t('Tag to identify the viewcounters.'),
    '#options' => $terms,
    '#default_value' => $form_values['tid'],
    '#required' => true,
  );
  foreach(module_implements("viewcounter_admin_wizard") as $module) {
    $module_choices = module_invoke($module, "viewcounter_admin_wizard", 'choices');
    foreach($module_choices as $key => $choice) {
      $choices[$module . "/" . $key] = $choice;
    }
  }
  $form['argtype'] = array(
    '#type' => 'radios',
    '#title' => t("Argument type"),
    '#description' => t('Select which method is used to pass arguments to the countable view.'),
    '#options' => $choices,
    '#default_value' => $form_values['argtype'],
    '#required' => true,
//     '#disabled' => isset($form_values['argtype']), //dosn't work sadly
  );
  if (isset($form_values['argtype'])) {
//     $form['argtype']['#type'] = 'markup';
    list($module, $choice) = explode("/", $form_values['argtype'], 2);
    $form += module_invoke($module, "viewcounter_admin_wizard_$choice",
      $form_values['form_step'], $form_values);
  }
  $form['form_step'] = array('#type' => 'hidden',
    '#value' => $form_values['form_step']['#value']+1);
  if ($form['form_finished']) {
    $form['countview'] = array(
      '#type' => 'select',
      '#title' => t("Countable view"),
      '#description' => t('The items in this view will be counted using each of the arguments created from the argument'),
      '#options' => _viewcounter_potential_references(array(),false),
      '#default_value' => $form_values['countview'],
      '#required' => true,
    );
    $form['countview_arguments'] = array(
      '#type' => 'textfield',
      '#title' => t("Arguments"),
      '#description' => t('Arguments to pass to this view. Separate arguments with <b>/</b> (forward slash).<br />'),
      '#default_value' => $form_values['countview_arguments'],
    );
    $form['form_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    $form['form_finished'] = array("#type" => "hidden", "#value" => true);
    $form['vcid'] = $form_values['vcid'] ? array("#type" => "hidden",
      "#value" => $form_values['vcid']) : NULL;

  } else {
    $form['form_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Next'),
    );
  }
  return $form;
}
function viewcounter_form_add_submit($form_id, &$form_values = NULL) {
//   dsm($form_values);
  if ($form_values['form_finished']) {
    $viewcounter = variable_get('viewcounter', array());
    $viewcounter['instances'] = ($viewcounter['instances'] ?
      $viewcounter['instances'] : array());
    if (!$form_values['vcid']) {
      foreach($viewcounter['instances'] as $v) {
        $myid = max($v['vcid'], $myid);
      }
      $myid++;
      $text = t("Created viewcounter #@vcid", array("@vcid" => $myid));
    } else {
      $myid = $form_values['vcid'];
      $text = t("Updated viewcounter #@vcid", array("@vcid" => $myid));
    }
    $viewcounter['instances'][$myid] = array();
    foreach($form_values as $k => $v) {
      if (strpos($k, "form_") === 0) {
        continue;
      }
      if ($k == "op") {
        continue;
      }
      $viewcounter['instances'][$myid][$k] = $v;
    }
    $viewcounter['instances'][$myid]['vcid'] = $myid;
    variable_set('viewcounter', $viewcounter);
//     dsm($viewcounter);
    drupal_set_message($text);
    return 'admin/settings/viewcounter';
  }
}
function viewcounter_viewcounter_admin_wizard($op, $arg1=NULL, $arg2=NULL) {
  switch($op) {
    case 'choices':
      return array(
        "static" => t("Single argument entered manually (or no argument)"),
        "views"  => t("Output of a view: each row in the view is passed as an argument"),
        "taxonomy" => t("Term IDs and names from vocabluary passed to the view"),
      );
  }
}
function viewcounter_viewcounter_admin_wizard_static($step, $form_values=NULL) {
  if ($step > 0) {
    $form['arguments'] = array(
      '#type' => 'textfield',
      '#title' => t("Arguments"),
      '#description' => t('Any arguments to pass to the view. Separate arguments with <b>/</b> (forward slash).<br />'),
      '#default_value' => $form_values['arguments'],
    );
  }
  if ($step > 1) {
    $form['form_finished'] = true;
  }
  return $form;
}
function viewcounter_viewcounter_admin_wizard_views($step, $form_values=NULL) {
  if ($step > 0) {
    $form['argumentview'] = array(
      '#type' => 'select',
      '#title' => t("Argument view"),
      '#description' => t('The output of this view is used as input for the countable view.<br />'
        . "This fould should return a title as first field, and any arguments to pass to the "
        . "countable view as arguments"),
      '#options' => _viewcounter_potential_references(array(),false),
      '#default_value' => $form_values['argumentview'],
      '#required' => true,
    );
    $form['argumentview_arguments'] = array(
      '#type' => 'textfield',
      '#title' => t("Arguments"),
      '#description' => t('Any arguments to pass to this view. Separate arguments with <b>/</b> (forward slash).<br />'),
      '#default_value' => $form_values['argumentview_arguments'],
    );
  }
  if ($step > 1) {
    $form['form_finished'] = true;
  }
  return $form;
}
function viewcounter_viewcounter_admin_wizard_taxonomy($step, $form_values=NULL) {
  if ($step > 0) {
    $vocs = taxonomy_get_vocabularies();
//     $vids = array(0 => '<'. t('None') .'>');
    foreach ($vocs as $vid => $vocab) {
      $vids[$vid] = $vocab->name;
    }
    $form['vocabulary'] = array(
      '#type' => 'select',
      '#title' => t("Vocabulary"),
      '#description' => t('The term ids in this vocabulary will be passed to teh countable view.'),
      '#options' => $vids,
      '#default_value' => $form_values['vocabulary'],
      '#required' => true,
    );
  }
  if ($step > 1) {
    $form['form_finished'] = true;
  }
  return $form;
}

