<?php
// $Id$
/**
 * @file
 * Views integration for <s>Revision Moderation</s>Viewcounter.
 */

/**
 * Implementation of hook_views_tables().
 */
function viewcounter_views_tables() {
  $tables['viewcounter'] = array(
    'name' => 'node_revisions',
    'provider' => 'internal',
    'join' => array(
      'left' => array(
          'table' => 'node',
          'field' => 'nid'
      ),
      'right' => array(
          'field' => 'nid',
      ),
    ),
    'fields' => array(
      'vid' => array(
        'name' => t('Node revision: ID'),
        'sortable' => TRUE,
        'help' => t('Display revision ID.'),
      ),
      'uid' => array(
        'name' => t('Node revision: Author ID'),
        'sortable' => TRUE,
        'help' => t('This will display the author id of the node revision.'),
      ),
      'name' => array(
        'name' => t('Node revision: Author name'),
        'handler' => 'views_handler_field_username',
        'notafield' => TRUE,
        'sortable' => FALSE,
        'option' => 'string',
        'uid' => 'uid',
        'addlfields' => array('uid'),
        'help' => t('This will display the author of the node revision.'),
      ),
      'title' => array(
        'name' => t('Node revision: Title'),
        'handler' => array(
          'viewcounter_handler_view_revision_link' => t('Normal'),
        ),
        'option' => array(
           '#type' => 'select',
           '#options' => array(
             'link' => 'As link',
             'nolink' => 'Without link'
            ),
        ),
        'vid' => 'vid',
        'addlfields' => array('vid'),
        'sortable' => FALSE,
        'help' => t('Display revision Title.'),
      ),
      'body' => array(
        'name' => t('Node revision: Body'),
        'handler' => array(
          'revision_moderation_handler_view_revision_link' => t('Normal'),
        ),
        'option' => array(
           '#type' => 'select',
           '#options' => array(
             'link' => 'As link',
             'nolink' => 'Without link'
            ),
        ),
        'vid' => 'vid',
        'addlfields' => array('vid'),
        'sortable' => FALSE,
        'help' => t('Display revision Body.'),
      ),
//       'state' => array(
//         'name' => t('Node revision: State'),
//         'handler' => 'revision_moderation_handler_revision_state',
//         'query_handler' => 'revision_moderation_query_handler_add_node_vid',
//         'notafield' => TRUE,
//         'option' => 'string',
//         'vid' => 'vid',
//         'addlfields' => array('vid'),
//         'sortable' => TRUE,
//         'help' => t('Display the state of the revision.'),
//       ),
//       'action' => array(
//         'name' => t('Node revision: Action'),
//         'handler' => 'revision_moderation_handler_revision_action',
//         'query_handler' => 'revision_moderation_query_handler_add_node_vid',
//         'notafield' => TRUE,
//         'option' => 'string',
//         'vid' => 'vid',
//         'addlfields' => array('vid'),
//         'sortable' => FALSE,
//         'help' => t('Display the revision actions.'),
//       ),
      'timestamp' => array(
        'name' => t('Node revision: Created time'),
        'sortable' => TRUE,
        'handler' => views_handler_field_dates(),
        'option' => 'string',
        'help' => t('Display revision created time.'),
      ),
    ),
// TODO: The sort on author name is not working due to the fact that the name
// is retrieved via a handler after the query is executed. Unless someone
// knows another trick, I commented the sort for the time being.
//     'sorts' => array(
//       'name' => array(
//         'name' => t('Node revision: Author Name'),
//         'field' => 'name',
//         'help' => t('This allows you to sort alphabetically by author.'),
//       )
//     ),
    'filters' => array(
      'uid' => array(
        'name' => t('Node revision: Author Name'),
        'operator' => 'views_handler_operator_or',
        'list' => 'views_handler_filter_username',
        'value-type' => 'array',
        'help' => t('This allows you to filter by a particular user. You might not find this useful if you have a lot of users.'),
      ),
      'timestamp' => array(
        'name' => t('Node revision: Created Time'),
        'operator' => 'views_handler_operator_gtlt',
        'value' => views_handler_filter_date_value_form(),
        'handler' => 'views_handler_filter_timestamp',
        'option' => 'string',
        'help' => t('This filter allows nodes to be filtered by their creation date.')
          .' '. views_t_strings('filter date'),
      ),
/*      'state' => array(
        'name' => t('Node revision: State'),
        'operator' => array('=' => 'equals'),
        'handler' => 'revision_moderation_state_filter',
        'list' => array('>' => 'Under moderation', '=' => 'Current', '<' => 'Old'),
        'value-type' => 'array',
        'help' => t('Only display revisions in the choosen state.'),
      ),*/
    ),
  );
  return $tables;
}



/**
 * Format a field as a link to a node revision.
 */
function viewcounter_handler_view_revision_link($fieldinfo, $fielddata, $value, $data) {
  if ($fielddata['options'] == 'nolink') {
    return check_plain($value);
  }
  return l($value, "node/$data->nid/revisions/$data->node_revisions_vid/view");
}
